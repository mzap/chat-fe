# Chat Frontend
App as frontend or client for chat. Using websocket with STOMP to pass messaging between clients and server.
#### Build
`docker build -t chat-fe .`
#### Run
`docker run -d -p 8080:8080 --name chat-fe chat-fe`
#### Log
`docker logs -f chat-fe`

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
